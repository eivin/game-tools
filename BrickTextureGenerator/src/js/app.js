var download = function(){
  var link = document.createElement('a');
  link.download = 'filename.png';
  link.href = document.getElementById('canvas').toDataURL()
  link.click();
}

import Vue from 'vue';

import App from './components/App.vue';
import BrickLayer from './paper/BrickLayer.js';
import params from './params.js';


new Vue({
	el: '#app',
	render: (createElement) => {
		return createElement( App )
	}
});

let brickLayer = new BrickLayer(params);

window.onload = function() {
	// Get a reference to the canvas object
	var canvas = document.getElementById('texture-view');
	// Create an empty project and a view for the canvas:
	paper.setup(canvas);
	paper.view.setViewSize(800,400);
	brickLayer.lay();
	paper.view.draw();
}
document.addEventListener('build', function (e) {
	brickLayer.lay();
	paper.view.draw();
}, false);
// window.paper = paper;