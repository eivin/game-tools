export default {
	gutter: {
		type: 'number',
		label: 'Gutter',
		step: 2,
		min: 0,
		max: 20,
		value: 4,
	},
	width: {
		type: 'number',
		label: 'Width',
		step: 2,
		min: 10,
		max: 200,
		value: 60,
	},
	normal: {
		type: 'checkbox',
		label: 'Normal',
		value: 1,
	},
	rows: {
		type: 'number',
		label: 'Rows',
		step: 1,
		min: 1,
		max: 200,
		value: 20,
	},
	cols: {
		type: 'number',
		label: 'Columns',
		step: 1,
		min: 1,
		max: 200,
		value: 10,
	},
	ratio: {
		type: 'percent',
		label: 'Ratio',
		step: 0.01,
		min: 0.1,
		max: 2,
		value: 0.5,
	},
	angle: {
		type: 'number',
		label: 'Angle',
		step: 1.0,
		min: 0,
		max: 360,
		value: 71,
	},
	randomAngle: {
		type: 'percent',
		label: 'Random angle',
		step: 0.05,
		min: 0,
		max: 1,
		value: 0.05,
	},

	randomizeColor: {
		type: 'percent',
		label: 'Randomize colours',
		step: 0.05,
		min: 0,
		max: 1,
		value: 0.1,
	},

	seed: {
		type: 'number',
		label: 'Seed',
		step: 1,
		min: 1,
		max: 100000,
		value: 16085,
	},

	colors: {
		type: 'colors',
		label: 'Colors',
		value: [
			{
				type: 'color',
				label: 'Color #1',
				value: '#8cb9f0',
			},
			{
				type: 'color',
				label: 'Color #2',
				value: '#fdf1ea',
			},
			{
				type: 'color',
				label: 'Color #3',
				value: '#fef7eb',
			}
		],
	},

	gutterColor: {
		type: 'color',
		label: 'Gutter color',
		value: '#5c4101',
	},
	gutterBleed: {
		type: 'percent',
		label: 'Gutter bleed',
		step: 0.05,
		min: 0,
		max: 1,
		value: 0.9,
	},

	gradientAmount: {
		type: 'percent',
		label: 'Gradient amount',
		step: 0.1,
		min: 0,
		max: 5,
		value: 1,
	},

	grainAmount: {
		type: 'percent',
		label: 'Grain amount',
		step: 0.1,
		min: 0,
		max: 1,
		value: 0.5,
	},


}