const c = {
	flat: '#807ffc',
	top: '#81f2ba',
	left: '#3081ba',
	right: '#ec7fba',
	bottom: '#823dba',
}

class Brick {
	constructor() {
		this.gutter = new paper.Shape.Rectangle({
			topLeft: [0,0],
			bottomRight: [100,100],
			// Fill the path with a gradient of three color stops
			// that runs between the two points we defined earlier:
			fillColor: '#cccccc'
		});
		this.path = new paper.Shape.Rectangle({
			topLeft: [0,0],
			bottomRight: [100,100],
			// Fill the path with a gradient of three color stops
			// that runs between the two points we defined earlier:
			fillColor: {
				gradient: {
					stops: ['#eeeeee','#999999','#999999']
				},
				origin: [0,0],
				destination: [100,100],
			}
		});

		this.normal = new paper.Group();

		this.normals = {
			flat: new paper.Shape.Rectangle({
				topLeft: [0,0],
				bottomRight: [100,100],
			}),
			top: new paper.Path(),
			left: new paper.Path(),
			right: new paper.Path(),
			bottom: new paper.Path(),
		};

		for (let x in this.normals) {
			let part = this.normals[x];
			part.addTo(this.normal);
			part.fillColor = c[x];

			if (x != 'flat') {
				part.add(new paper.Segment());
				part.add(new paper.Segment());
				part.add(new paper.Segment());
				part.add(new paper.Segment());
			}
		}
	}

	remove() {
		this.gutter.remove();
		this.path.remove();
		this.normal.remove();
	}
	addTo(target, normal) {
		if (normal) {
			this.normal.addTo(target);
		} else {
			this.gutter.addTo(target);
			this.path.addTo(target);
		}
	}

	mirror(brick) {
		this.gutter.remove();
		this.gutter = brick.gutter.clone();
		this.path.remove();
		this.path = brick.path.clone();
		this.updateNormals();
	}

	setPos(pos) {
		this.path.position = new paper.Point(pos);
		let gw = this.gutter.size.width - this.path.size.width;
		this.gutter.position = new paper.Point(pos[0] + gw/2, pos[1] + gw/2);
		this.updateNormals();
	}

	updateNormals() {

		this.normals.flat.setPosition(
			this.gutter.getPosition()
		);
		this.normals.flat.size = this.gutter.getSize();

		let rel = {
			bottom: [
				new paper.Point(-1.0,  1.0),
				new paper.Point( 1.0,  1.0),
				new paper.Point( 0.8,  0.8),
				new paper.Point(-0.8,  0.8),
			],
			left: [
				new paper.Point( -1.0,  1.0),
				new paper.Point( -0.8,  0.8),
				new paper.Point( -0.8, -0.8),
				new paper.Point( -1.0, -1.0),
			],
			right: [
				new paper.Point( 1.0,  1.0),
				new paper.Point( 1.0, -1.0),
				new paper.Point( 0.8, -0.8),
				new paper.Point( 0.8,  0.8),
			],
			top: [
				new paper.Point(-0.8, -0.8),
				new paper.Point(-1.0, -1.0),
				new paper.Point( 1.0, -1.0),
				new paper.Point( 0.8, -0.8),
			],
		};

		for (let x in rel) {
			let segments = this.normals[x].getSegments();
			for (let i = 0; i < segments.length; i++) {
				let segment = segments[i];
				let size = this.path.getSize().divide(2);
				let mag = new paper.Point(size);
				let pos = this.path.getPosition();
				let newPos = pos.add(mag.multiply(rel[x][i]));
				segment.point = newPos;
			}
		}

	}

	setColor(color, angle, strength) {
		let w = this.path.size.width;
		let h = this.path.size.height;

		let pos = this.path.getPosition();

		let dx = w/2, dy = h/2;
		if (dx === 0) {
			dx = 0.1;
		}
		if (dy === 0) {
			dy = 0.1;
		}

		let p = new paper.Point(w, h);
		p.angle = angle;
		this.path.fillColor.origin = pos.subtract(p);
		this.path.fillColor.destination = pos.add(p);

		let c1 = new paper.Color(color);
		let c2 = new paper.Color(color);
		let c3 = new paper.Color(color);
		c2.lightness = Math.pow(
			c2.lightness, 1 - 0.2 * strength
		) + strength * 0.02;
		c3.lightness = Math.pow(
			c3.lightness, 1 + 0.2 * strength
		) - strength * 0.07;

		this.path.fillColor.gradient.stops[0].setColor(c1);
		this.path.fillColor.gradient.stops[1].setColor(c2);
		this.path.fillColor.gradient.stops[2].setColor(c3);
	}
	lerp (start, end, amt){
		return (1-amt)*start+amt*end
	}
	setGutterColor(color, bleed) {
		let originalColor = this.path.fillColor.gradient.stops[0].getColor();
		let c1 = new paper.Color(color);

		this.gutter.fillColor = new paper.Color(
			this.lerp(c1.red, originalColor.red, bleed),
			this.lerp(c1.green, originalColor.green, bleed),
			this.lerp(c1.blue, originalColor.blue, bleed)
		);
	}

	setSize(w, h, gutter) {
		this.gutter.size = [w + gutter, h + gutter];
		this.path.size = [w, h];
	}
}

export default Brick;