import Brick from './Brick.js';
import params from '../params.js';
import RNG from '../rng.js';

class BrickLayer {
	constructor() {
		this.pool = [];
	}

	lay() {
		this.rng = new RNG(params.seed.value);
		let pool = this.pool;
		let rows = params.rows.value;
		let cols = params.cols.value;
		let width = params.width.value;
		let height = width * params.ratio.value;
		let gutter = parseFloat(params.gutter.value);
		let normal = params.normal.value;

		// Remove others.
		for (var i = 0; i < pool.length; i++) {
			for (var j = 0; j < pool[i].length; j++) {
				pool[i][j].remove();
			}
		}

		for (var i = 0; i < rows; i++) {
			let isOffset = i % 2;
			let n = cols;
			if (typeof pool[i] === 'undefined') {
				// console.log('Adding new row, ' + i);
				pool[i] = [];
			}
			// console.log('Laying ' + n + ' bricks on row ' + i);
			for (var j = 0; j < n; j++) {
				let brick = this.getBrick(i, j);
				this.layBrick(
					pool[i][j],
					this.getWorldPos(i, j, isOffset)
				);
				pool[i][j].addTo(paper.project, normal);

			}

			if (isOffset) {
				let brick = this.getBrick(i, j);
				brick.mirror(
					pool[i][0]
				);
				brick.setPos(
					this.getWorldPos(i, n, isOffset)
				);
				pool[i][j].addTo(paper.project, normal);
			}
		}

		let w = cols * width + cols * gutter;
		let h = rows * height + rows * gutter;
		paper.view.setViewSize(w,h);
	}

	getBrick(i, j) {
		if (typeof this.pool[i][j] === 'undefined') {
			// console.log('Adding new brick, ' + j + ', on row ' + i);
			this.pool[i][j] = new Brick;
		}
		return this.pool[i][j];
	}

	layBrick(brick, pos) {
		let width  = parseFloat(params.width.value);
		let height = width * parseFloat(params.ratio.value);
		let gutter = parseFloat(params.gutter.value);
		brick.setSize(
			width,
			width * params.ratio.value,
			gutter
		);

		brick.setPos(pos);

		let colors = params.colors.value;
		let color = new paper.Color(this.rng.choice(colors).value);
		let angle = parseFloat(params.angle.value) + this.rng.nextFloat() * parseFloat(params.randomAngle.value) * 360;
		let rc = parseFloat(params.randomizeColor.value);
		let r1 = (this.rng.nextFloat() * rc) - rc/2;
		let r2 = (this.rng.nextFloat() * rc) - rc/2;
		let r3 = (this.rng.nextFloat() * rc) - rc/2;

		color.saturation += r1 * 10;
		if (color.saturation > 2) {
			color.saturation = 2;
		}
		if (color.saturation < 0) {
			color.saturation = 0;
		}
		color.hue        += r2 * 255;
		color.lightness  += r3 * 0.3;
		// console.log(color);
		brick.setColor(
			color,
			angle,
			parseFloat(params.gradientAmount.value)
		);

		brick.setGutterColor(
			params.gutterColor.value,
			parseFloat(params.gutterBleed.value)
		);

	}

	getWorldPos(i, j, isOffset) {
		let w = parseFloat(params.width.value);
		let x = j * w + j * parseFloat(params.gutter.value);
		let h = w * parseFloat(params.ratio.value);
		let y = i * h + i * parseFloat(params.gutter.value);
		if (isOffset) {
			x -= params.width.value / 2;
		}
		return [x + w/2, y + h/2];
	}
}


export default BrickLayer;